
# You can install mods by referring to the following:
https://openmw.readthedocs.io/en/latest/reference/modding/mod-install.html#install

# Or if you're lazy DOUG
- open your OpenMW config file: `C:\Users\{YourStupidName}\Documents\My Games\OpenMW\openmw.cfg`
- Add the following line on (line 570 for me) directly below the line that reads "data" (double quotes are required)
    `data="C:\Users\stuar\OneDrive\Desktop\MWMP\MODS\Data"`




# Weapon sheathing:
If using the OpenMW engine, you will need to enable the weapon sheathing option in the launcher (under Advanced->Visuals->Animations).
    Place the contents of the mod archive into your "/Morrowind/Data Files/" directory.
    Add the following to your settings.cfg file:
        [Game]
        weapon sheathing = true
        use additional anim sources = true


# Toggle sneak
edit openmw settings.cfg
toggle sneak = true
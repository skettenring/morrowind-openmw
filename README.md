# Morrowind OpenMW Multiplayer
# Setup

Setting up OpenMW Multiplayer is a fairly simple process.
*Ensure you have a clean installation of Morrowind before beginning*

1. Install Morrowind

2. Run setup
    * `setup_multiplayer.exe`
    * Follow the instructions

# TO PLAY
* Open `Play Morrowind Multiplayer` shortcut
* Morrowind should open and load up
* Enter your character's name
* Log in with the server password

You will need to enter your player name and password each time you log in, but everything you do will be saved by the server.

... _everything_ ...

# References
* OpenMW: https://openmw.org/en/
* TES3MP: https://tes3mp.com/
* TES3MP Git: https://github.com/TES3MP/TES3MP/wiki/Quickstart-guide
* Installing Mods: https://openmw.readthedocs.io/en/latest/reference/modding/mod-install.html#install
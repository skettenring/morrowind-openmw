# Set up OpenMW MP
./tes3mp/openmw-wizard.exe

Start-Sleep -Seconds 1
Write-Output "Finish setting the OpenMW target Morrowind location then proceed with setup"
Start-Sleep -Seconds 5

# setting some variables
$documents_path = [Environment]::GetFolderPath("MyDocuments")
$openMW_settingsPath = Join-Path -Path $documents_path -ChildPath "My Games\OpenMW\settings.cfg"
$openMW_configPath = Join-Path -Path $documents_path -ChildPath "My Games\OpenMW\openmw.cfg"
$absolute_path = Get-Location
$replacementSettingsPath = Join-Path -Path $absolute_path -ChildPath ".temp/replacement-settings.cfg"
$replacementConfigPath = Join-Path -Path $absolute_path -ChildPath ".temp/replacement-openmw.cfg"
$replacementClientConfigPath = Join-Path -Path $absolute_path -ChildPath ".temp/replacement-client-default.cfg"
$openmwClientConfigPath = Join-Path -Path $absolute_path -ChildPath "tes3mp/tes3mp-client-default.cfg"

# prompt for host ip
$ipv4 = '^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'

do
{
    $hostip = Read-Host 'Enter the ip address of the coolest dude in town'
} while ($hostip -notmatch $ipV4)

# get bank account and transfer funds
# overwrite openmw.cfg
Write-Output "Overwriting openMW config..."
$placeholder = '$openmwmp$'
$openmwContent = Get-Content -path $replacementConfigPath -Raw
$openmwContent = $openmwContent.replace($placeholder, $absolute_path)
Set-Content -Path $openMW_configPath -Value $openmwContent

$correctAnswer = '(?i:y)'
do
{
    $answer1 = Read-Host "Wouldn't you agree that Stu is the coolest ever? Godlike even? (y/n)"
} while ($answer1 -notmatch $correctAnswer)


# overwrite settings.cfg
Write-Output "Overwriting openMW settings..."
Get-Content -path $replacementSettingsPath -Raw | Set-Content -Path $openMW_settingsPath

do
{
    $answer2 = Read-Host "Don't you think that Stu is pretty awesome? (y/n)"
} while ($answer2 -notmatch $correctAnswer)

# judge harshly
Write-Output "Taking a look at your browser history..."
for (($i = 0); $i -lt 5; $i++)
{
    start-sleep -Seconds 1
    Write-Output "..."
}

start-sleep -Seconds 1
Write-Output "Yikes. Who knew you liked tentacles so much. Moving on."
start-sleep -Seconds 1

# overwrite client settings
$ip_placeholder = '$hostip$'

Write-Output "Overwriting default client settings..."
$clientConfigContent = Get-Content -path $replacementClientConfigPath -Raw
$clientConfigContent = $clientConfigContent.replace($ip_placeholder, $hostip)
Set-Content -Path $openmwClientConfigPath -Value $clientConfigContent

$correctAnswer = '(?i:y)'
do
{
    $answer3 = Read-Host "Do you submit to the will of our dark lord and master Cthulhu? (y/n)"
} while ($answer3 -notmatch $correctAnswer)

Read-Host "Setup complete. Press any key to exit and don't forget to tell Stu he's fucking awesome for writing this script"